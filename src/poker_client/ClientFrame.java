package poker_client;

import poker.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

/**
 * A frame class for the Tic-Tac-Toe client
 * @author tcolburn
 */
public class ClientFrame extends JFrame {
    
    public ClientFrame() {
        super("The Texas Hold'em Client");
        
        bPanel = new ButtonPanel(this);
        mPanel = new MessagePanel();
        tPanel = new TablePanel(new Table());

        OpeningDialog opener = new OpeningDialog(this);
        opener.setVisible(true);

        bPanel.setBounds(0, 400, 500, 200);
        mPanel.setBounds(500, 400, 300, 200);
        tPanel.setBounds(0, 0, 800, 400);

        add(bPanel);
        add(mPanel);
        add(tPanel);
        
        setSize(816,638);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        
        addWindowListener(new WindowAdapter() {
           public void windowClosed(WindowEvent e) {
               client.quit();
           }
        });
    }

    public void display(Object msg)
    {
        if (msg instanceof String) {
            String message = (String) msg;
            mPanel.addMessage(message);
            this.repaint();
        }
        else if ( msg instanceof Table ) {
            tPanel.updateTable((Table) msg);
            this.repaint();
        }
        else
            throw new RuntimeException("Unrecognized message type in handleMessageFromServer");
    }

    public String getClientInput()
    {
        return "";
    }

    public void pass(Move move) {
        client.handleInputFromClientUI(move);
    }
    /**
     * Mutator for the player's name.
     * @param newVal the player's name.
     */
    public void setName(String newVal) {
        name = newVal;
    }
    
    /**
     * Mutator for the server host.  The data panel is also notified.
     * @param newVal the server's IP address or domain name.
     */
    public void setHost(String newVal) {
        host = newVal;
        //dataPanel.setHost(newVal);
    }
    
    
    public void initialize() {
        try
        {
	client = new PokerClient(host, 5555, this);
        }
        catch (IOException exp)
        {
            System.err.println("Error:ClientFrame-Initialize");
        }
        client.handleInputFromClientUI(name);
        setVisible(true);
    }

    public static void main(String[] args) {
        new ClientFrame();
    }
  
    private String name = "";
    private String host;
    private PokerClient client;
    ButtonPanel bPanel;
    MessagePanel mPanel;
    PokerEngine engine;
    TablePanel tPanel;
}
