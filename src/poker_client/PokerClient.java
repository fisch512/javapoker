package poker_client;

import ocsf.client.*;
import poker.*;
import java.io.*;

public class PokerClient extends AbstractClient
{
    public PokerClient(String host, int port, ClientFrame clientUI)
	throws IOException 
    {
	super(host, port); //Call the superclass constructor
	this.clientUI = clientUI;
	openConnection();
    }

    public String getName()
    {
        return clientUI.getName();
    }

    public void handleMessageFromServer(Object msg) 
    {
        if (msg instanceof String)
        {
            String message = (String) msg;
            clientUI.display(message);
        }
        else if ( msg instanceof Table ) {
            clientUI.display((Table) msg);
        }
        else // Shouldn't happen
            throw new RuntimeException("Unrecognized message type in handleMessageFromServer");
    }

    /**
     * This method handles all data coming from the user interface
     * by simply sending it on to the server.
     * @param message The message the player wishes to send to the server.
     */
    public void handleInputFromClientUI(Object message)
    {
	try {
	    sendToServer(message);
	}
	catch(IOException e) {
	    clientUI.display("Error sending message to server.  Terminating client.");
	    quit();
	}
    }
  
    /**
     * This method terminates the client by closing the connection to the
     * server and exiting.
     */
    public void quit()
    {
	try
	    {
		closeConnection();
	    }
	catch(IOException e)
        {
            System.err.println("Error:PokerClient-quit");
        }
    }
    
    protected void connectionClosed() {
//        clientUI.display("Connection closed.");
    }
    
    protected void connectionException(Exception exception) {
      //  clientUI.display("Connection exception: " + exception.getMessage());
    }

    private ClientFrame clientUI;

}
//End of TicTacToeClient class
