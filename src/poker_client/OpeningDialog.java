package poker_client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class OpeningDialog extends JDialog {
    
    public OpeningDialog(final ClientFrame frame) {
        super(frame, "Welcome to Texas Hold'em");
        
//  setModalityType only works in 1.6        
//        setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        
        setModal(true);
        
        Container contentPane = getContentPane();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        
        nameField = new JTextField(20);
        hostField = new JTextField("localhost", 20);
        
        final OpeningDialog dialog = this;
        
        joinButton = new JButton("Join Game");
        joinButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent event) {
               frame.setName(getPlayerName());
               frame.setHost(getHost());
               dialog.dispose();
               frame.initialize();
           }});
        
        JPanel namePanel = new JPanel();
        namePanel.add(new Label("Your Name: "));
        namePanel.add(nameField);
        
        JPanel hostPanel = new JPanel();
        hostPanel.add(new Label("Server Host: "));
        hostPanel.add(hostField);
        
        contentPane.add(namePanel);
        contentPane.add(hostPanel);
        contentPane.add(joinButton);
        
        setSize(400, 250);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
    
    private String getPlayerName() {
        return nameField.getText();
    }
    
    private String getHost() {
        return hostField.getText();
    }
    
    private JTextField nameField;
    private JTextField hostField;
    
    private JButton joinButton;

}
