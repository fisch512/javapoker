package poker;

import javax.swing.*;
import java.util.*;
import java.io.*;

/**
 * Class that represents a hand of playing cards.
 * @author Matt
 */
public class Hand implements Serializable, Cloneable{

    /**
     * The public constructor for a hand of playing cards. Two individual cards
     * and five community cards.
     * @param card1 First individual card
     * @param card2 Second individual card
     * @param comCards List of the five community cards.
     */
    public Hand(Card card1, Card card2, ArrayList<Card> comCards)
    {
        ArrayList<Card> temp = new ArrayList(comCards.size());
        temp.addAll(comCards);

        this.card1 = card1;
        this.card2 = card2;
        this.comCards = comCards;
        allCards = temp;
        allCards.add(card1);
        allCards.add(card2);
    }

    public Hand clone()
    {
        try
        {
            Hand clonedHand = (Hand) super.clone();
            clonedHand.card1 = (Card) card1.clone();
            clonedHand.card2 = (Card) card2.clone();

            ArrayList<Card> c = new ArrayList();
            for(Card o : comCards)
            {
                c.add(o.clone());
            }
            //c.addAll(comCards);
            clonedHand.comCards = c;

            ArrayList<Card> a = new ArrayList();
            for(Card o : allCards)
            {
                a.add(o.clone());
            }
            //a.addAll(allCards);
            clonedHand.allCards = a;



//            ArrayList<Card> c = new ArrayList(5);
//            for(int i = 0; i < 5; i++)
//            {
//                c.set(i, (Card)comCards.get(i).clone());
//            }
//            clonedHand.comCards = c;
//
//            ArrayList<Card> a = new ArrayList(7);
//            for(int j = 0; j < 7; j++)
//            {
//                a.set(j, (Card)allCards.get(j).clone());
//            }
//            clonedHand.allCards = a;

            return clonedHand;


        }
        catch (CloneNotSupportedException e)
        {
            System.err.println("Error: Hand Clone");
            return null;
        }
    }

    /**
     * Returns the first individual card.
     * @return card1
     */
    public Card getCard1()
    {
        return card1;
    }

    /**
     * Returns the second individual card.
     * @return card2
     */
    public Card getCard2()
    {
        return card2;
    }

    public ArrayList<Card> getComCards()
    {
        return comCards;
    }

    private Card card1;
    private Card card2;
    private ArrayList<Card> comCards;
    private ArrayList<Card> allCards;

}
