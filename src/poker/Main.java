/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package poker;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 *
 * @author Matt
 */
public class Main {
    public static void main(String[] args)
    {
        ImageIcon icon = new ImageIcon("src/classic-cards/" + 1 + ".png");
        Card c = new Card(14, "Heart", icon, true);

        JFrame frame = new JFrame("Test");
        JLabel label = new JLabel(c.getCard());
        frame.add(label);
        frame.pack();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }

}
