package poker;

import java.awt.Color;
import javax.swing.*;

public class ControlFrame extends JFrame {
    
   
    public ControlFrame() {
        super("Texas Hold'em");
        add(new ControlPanel());
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setVisible(true);        
    }
    
   
    public static void main(String[] args) {
        new ControlFrame();
    }

}
