/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package poker;

import poker_client.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.*;
import java.io.*;
import java.util.*;

/**
 *
 * @author Matt
 */
public class MessagePanelTester {
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("TablePanel Tester");
        MessagePanel panel = new MessagePanel();
        frame.add(panel);
        frame.pack();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setVisible(true);

        for(int i = 0; i < 30; i++)
        {
            try
            {
                Thread.currentThread().sleep(500);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            panel.addMessage("Player " + i + " folds");
        }
    }

}
