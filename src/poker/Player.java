
package poker;
import java.util.*;
import java.io.*;

/**
 * Class that represents a player playing poker.
 * @author Matt
 */
public class Player implements Serializable, Cloneable {

    private final static int startChips = 5000;

    /**
     * Public constructor for a player object.
     * @param name The name of the player
     * @param chips The starting ammount of chips
     */
    public Player(String name)
    { 
        chips = startChips;
        this.name = name;
    }

    public Player clone()
    {
        try
        {
            Player clonedPlayer = (Player)super.clone();
            clonedPlayer.hand = (Hand) hand.clone();
            return clonedPlayer;
        }
        catch (CloneNotSupportedException e)
        {
            return null;
        }
    }


    /**
     * Returns the ammount of chips the player has.
     * @return the ammount of chips the player has.
     */
    public int getChips()
    {
        return chips;
    }

    public ArrayList<Card> getFullHand()
    {
        ArrayList<Card> fullHand = new ArrayList(7);
        fullHand.addAll(getHand().getComCards());
        fullHand.add(getHand().getCard1());
        fullHand.add(getHand().getCard2());

        return fullHand;
    }
    public void takeChips(int x)
    {
        chips -= x;
    }
    /**
     * Returns the players hand.
     * @return the players hand as a list.
     */
    public Hand getHand()
    {
        return hand;
    }

    /**
     * Returns the players name
     * @return player name
     */
    public String getName()
    {
        return name;
    }

    /**
     * The player wants to bet during their turn.
     * @param ammount the ammount of the bet they would like to place.
     */
    public void bet(int ammount)
    {

    }

    /**
     * The player wants to check during their turn.
     */
    public void check()
    {

    }

    /**
     * The player wants to call during their turn.
     */
    public void call()
    {

    }

    /**
     * The player wants to raise during their turn.
     * @param ammount the ammount the player would like to raise.
     */
    public void raise(int ammount)
    {
    }

    /**
     * The player wants to fold during their turn.
     */
    public void fold()
    {

    }

    /**
     * Adds chips to the players chip ammount.
     * @param ammount the ammount of chips to add.
     */
    public void addChips(int ammount)
    {
        chips += ammount;
    }

    public void setHand(Hand hand)
    {
        this.hand = hand;
    }

    private int chips;
    private String name;
    private Hand hand;
}
