package poker;

import java.awt.image.BufferedImage;
import javax.imageio.*;
import java.io.*;
import javax.swing.ImageIcon;

/**
 * Class that represents a single playing card.
 * @author Matt
 */
public class Card implements Serializable, Cloneable{

    /**
     * The public constructor for a card that sets its values.
     * @param value Represents the number on the card; Jack = 11, Queen = 12,
     * King = 13, Ace = 14, 2-10 = 2-10.
     * @param suit Represents the suit of the card: Heart, Diamond, Spade, Club.
     * @param faceIcon The picture of the card.
     * @param isFlippedUp Represents whether the card is flipped up or down.
     */
    Card(int value, String suit, ImageIcon faceIcon, boolean isFlippedUp)
    {
            backIcon = new ImageIcon("src/classic-cards/b1fv.png");

            this.value = value;
            this.suit = suit;
            this.faceIcon = faceIcon;
            this.flippedUp = isFlippedUp;

            if(flippedUp)
                card = faceIcon;
            else card = backIcon;
    }

    public Card clone()
    {
        try
        {
            Card clonedCard = (Card) super.clone();
            return clonedCard;
        }
        catch (CloneNotSupportedException e)
        {
            return null;
        }
    }

    /**
     * Returns the icon representing the card.
     * @return the icon of the card showing either the face or back depending
     * on the boolean value isFlippedUp.
     */
    public ImageIcon getCard()
    {
        return card;
    }

    /**
     * Returns the value of the card.
     * @return the value of the card.
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Returns the suit of the card.
     * @return the suit of the card.
     */
    public String getSuit()
    {
        return suit;
    }

    /**
     * Flips the card over changing the icon of the card to the oppisite side.
     */
    public void flipCard()
    {
        if(isFlippedUp())
        {
            flippedUp = false;
            card = backIcon;
        }
        else
        {
            flippedUp = true;
            card = faceIcon;
        }
        
    }

    public boolean isFlippedUp()
    {
        return flippedUp;
    }

    private ImageIcon card;
    private ImageIcon faceIcon;
    private ImageIcon backIcon = null;

           // new Image(Main.class.getResource("/classic-cards/b1fv.png"));
    //Jack being 11, Queen being 12, King being 13, and Ace being 14
    private int value;
    private String suit;
    private boolean flippedUp;
}
