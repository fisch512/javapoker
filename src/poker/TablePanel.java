package poker;

import poker_client.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.*;
import java.io.*;
import java.util.*;

/**
 * A class that draws the graphics for the poker table.
 * @author Matt
 */
public class TablePanel extends JPanel {
    public TablePanel(Table t)
    {
        table = t;
        setLayout(null);
        setSize(800, 400);
        setBackground(Color.PINK);

        player1 = new JLabel("Player1");
        player2 = new JLabel("Player2");
        player3 = new JLabel("Player3");
        player4 = new JLabel("Player4");
        pot = new JLabel("Pot: " + table.getPot());

        player1.setBounds(360, 335, 140, 65);
        player2.setBounds(20,168, 140, 65);
        player3.setBounds(360, 0, 140, 65);
        player4.setBounds(680, 168, 140, 65);
        pot.setBounds(380, 220, 140, 65);

        add(player1);
        add(player2);
        add(player3);
        add(player4);
        add(pot);

        if(!table.getIsEmpty())
        {
            player1.setText(table.getPlayer1().getName() + " $" +
                    table.getPlayer1().getChips());
            player2.setText(table.getPlayer2().getName() + " $" +
                    table.getPlayer2().getChips());
            player3.setText(table.getPlayer3().getName() + " $" +
                    table.getPlayer3().getChips());
            player4.setText(table.getPlayer4().getName() + " $" +
                    table.getPlayer4().getChips());
            pot.setText("Pot: " + table.getPot());
        }
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.getHSBColor(21F, 0.67F, 0.33F));
        g2.fillOval(140, 65, 520, 270);
        g2.setColor(Color.green);
        g2.fillOval(150, 75, 500, 250);

        if(!table.getIsEmpty())
        {
            g2.drawImage(table.getPlayer1().getHand().getCard1().getCard().getImage(),
                    410-(44), 325-(60) , 44, 60, null);
            g2.drawImage(table.getPlayer1().getHand().getCard2().getCard().getImage(),
                    410-(44/2), 325-(60) , 44, 60, null);
            g2.drawImage(table.getPlayer2().getHand().getCard1().getCard().getImage(),
                    155, 200-(60/2) , 44, 60, null);
            g2.drawImage(table.getPlayer2().getHand().getCard2().getCard().getImage(),
                    155+(44/2), 200-(60/2) , 44, 60, null);
            g2.drawImage(table.getPlayer3().getHand().getCard1().getCard().getImage(),
                    410-(44), 20+(60) , 44, 60, null);
            g2.drawImage(table.getPlayer3().getHand().getCard2().getCard().getImage(),
                    410-(44/2), 20+(60) , 44, 60, null);
            g2.drawImage(table.getPlayer4().getHand().getCard1().getCard().getImage(),
                    645-(44)-(44/2), 200-(60/2) , 44, 60, null);
            g2.drawImage(table.getPlayer4().getHand().getCard2().getCard().getImage(),
                    645-(44), 200-(60/2) , 44, 60, null);

            g2.drawImage(table.getComCards().get(0).getCard().getImage(),
                    233, 195-(84/2), 63, 84,null);
            g2.drawImage(table.getComCards().get(1).getCard().getImage(),
                    301, 195-(84/2), 63, 84,null);
            g2.drawImage(table.getComCards().get(2).getCard().getImage(),
                     400-31, 195-(84/2), 63, 84,null);
            g2.drawImage(table.getComCards().get(3).getCard().getImage(),
                     437, 195-(84/2), 63, 84,null);
            g2.drawImage(table.getComCards().get(4).getCard().getImage(),
                     505, 195-(84/2),63, 84,null);
        }

    }
    
    public void updateTable(Table t)
    {
        table = t;
        if(!table.getIsEmpty())
        {
            repaint();
            player1.setText(table.getPlayer1().getName() + " $" +
                    table.getPlayer1().getChips());
            player2.setText(table.getPlayer2().getName() + " $" +
                    table.getPlayer2().getChips());
            player3.setText(table.getPlayer3().getName() + " $" +
                    table.getPlayer3().getChips());
            player4.setText(table.getPlayer4().getName() + " $" +
                    table.getPlayer4().getChips());
            pot.setText("Pot: " + table.getPot());
        }
    }

    private Table table;
    private JLabel player1;
    private JLabel player2;
    private JLabel player3;
    private JLabel player4;
    private JLabel pot;
}
