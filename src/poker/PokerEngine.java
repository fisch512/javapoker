
package poker;

import java.io.*;
import java.util.*;
import poker_client.PokerClient;
import poker_server.PokerServer;

/**
 *
 * @author Matt
 */
public class PokerEngine {

    public class PlayOutOfTurnException extends RuntimeException { }

    public class BadMoveException extends RuntimeException { }

    public class PlayWhenFoldedException extends RuntimeException { }

    public static final int SBLIND = 25;
    public static final int BBLIND = 50;

    public PokerEngine(PokerServer server)
    {
        this.server = server;
    }

    public void attemptMove(Move move, String player)
    {
        String name;

        if(player.equals("player1"))
            name = p1.getName();
        else if(player.equals("player2"))
            name = p2.getName();
        else if(player.equals("player3"))
            name = p3.getName();
        else name = p4.getName();

        if (!player.equals(nextToPlay))
        {
	    throw new PlayOutOfTurnException();
	}
        else if((player.equals("player1") && p1HasFolded) ||
                (player.equals("player2") && p2HasFolded) ||
                (player.equals("player3") && p3HasFolded) ||
                (player.equals("player4") && p4HasFolded))
        {
            throw new PlayWhenFoldedException();
        }
        else if(move.getMove().equals("fold"))
        {
            if(player.equals("player1"))
                p1HasFolded = true;
            else if(player.equals("player2"))
                p2HasFolded = true;
            else if(player.equals("player3"))
                p3HasFolded = true;
            else p4HasFolded = true;
            server.sendToAll(name + " has folded");
            toggleNextToPlay();
        }
        else if(move.getMove().equals("checkCall"))
        {
            if(moves.isEmpty() || currentBet == 0)
            {
                moves.add(0, move);
                server.sendToAll(name + " has checked");
                toggleNextToPlay();
            }
            else
            {
                moves.add(0, move);
                if(player.equals("player1"))
                {
                     p1.takeChips(currentBet - p1PrevBet);
                     table.addToPot(currentBet - p1PrevBet);
                     p1PrevBet = currentBet;
                }
                else if(player.equals("player2"))
                {
                     p2.takeChips(currentBet - p2PrevBet);
                     table.addToPot(currentBet - p2PrevBet);
                     p2PrevBet = currentBet;
                }
                else if(player.equals("player3"))
                {
                     p3.takeChips(currentBet - p3PrevBet);
                     table.addToPot(currentBet - p3PrevBet);
                     p3PrevBet = currentBet;
                     
                }
                else
                {
                    p4.takeChips(currentBet - p4PrevBet);
                    table.addToPot(currentBet - p4PrevBet);
                    p4PrevBet = currentBet;
                    
                }

                server.sendToAll(name + " has called");
                sendToAll(table);
                toggleNextToPlay();
            }
        }
        else if(move.getMove().equals("betRaise"))
        {
            int x = 0;
            if(player.equals("player1"))
                x = p1.getChips();
            else if(player.equals("player2"))
                x = p2.getChips();
            else if(player.equals("player3"))
                x = p3.getChips();
            else x = p4.getChips();

            if((x < move.getAmt()) || (move.getAmt() < 0))
            {
                throw new BadMoveException();
            }
            else if(currentBet == 0)
            {
                moves.add(0, move);
                currentBet = move.getAmt();
                if(player.equals("player1"))
                {
                     p1.takeChips(currentBet);
                     p1PrevBet = currentBet;
                }
                else if(player.equals("player2"))
                {
                     p2.takeChips(currentBet);
                     p2PrevBet = currentBet;
                }
                else if(player.equals("player3"))
                {
                     p3.takeChips(currentBet);
                     p3PrevBet = currentBet;
                }
                else
                {
                    p4.takeChips(currentBet);
                    p4PrevBet = currentBet;
                }

                table.addToPot(currentBet);
                server.sendToAll(name + " bets $" + currentBet);
                sendToAll(table);
                toggleNextToPlay();
            }
            else
            {
                moves.add(0, move);
                currentBet += move.getAmt();
                if(player.equals("player1"))
                {
                     p1.takeChips(currentBet - p1PrevBet);
                     table.addToPot(currentBet - p1PrevBet);
                     p1PrevBet = currentBet;
                     
                }
                else if(player.equals("player2"))
                {
                     p2.takeChips(currentBet - p2PrevBet);
                     table.addToPot(currentBet - p2PrevBet);
                     p2PrevBet = currentBet;
                     
                }
                else if(player.equals("player3"))
                {
                     p3.takeChips(currentBet - p3PrevBet);
                     table.addToPot(currentBet - p3PrevBet);
                     p3PrevBet = currentBet;
                     
                }
                else
                {
                    p4.takeChips(currentBet - p4PrevBet);
                    table.addToPot(currentBet - p4PrevBet);
                    p4PrevBet = currentBet;
                    
                }

                server.sendToAll(name + " Raises $" + move.getAmt() +
                        " raising the current bet to $" + currentBet);
                sendToAll(table);
                toggleNextToPlay();
            }
        }
    }

    private void toggleNextToPlay()
    {
        int folded = 0;
        if(p1HasFolded)
            folded++;
        if(p2HasFolded)
            folded++;
        if(p3HasFolded)
            folded++;
        if(p4HasFolded)
            folded++;

        if(folded == 3)
        {
            p1PrevBet = 0;
            p2PrevBet = 0;
            p3PrevBet = 0;
            p4PrevBet = 0;
            currentBet = 0;
            sendToAll(table);
            sleep(1000);
            done = true;
            moves.clear();
        }
        else if(roundOfBetting == 1 && (moves.size() > 4) && folded == 0 &&
                moves.get(0).getMove().equals("checkCall") &&
                    moves.get(1).getMove().equals("checkCall") &&
                     moves.get(2).getMove().equals("checkCall") &&
                     moves.get(3).getMove().equals("checkCall") &&
                     moves.get(4).getMove().equals("betRaise"))
        {
            roundOfBetting();
        }
        else if(roundOfBetting == 1 && (moves.size() > 3) && folded == 1 &&
            moves.get(0).getMove().equals("checkCall") &&
                    moves.get(1).getMove().equals("checkCall") &&
                    moves.get(2).getMove().equals("checkCall") &&
                    moves.get(3).getMove().equals("betRaise"))
        {
            roundOfBetting();
        }
        else if (roundOfBetting == 1 && (moves.size() > 2) && folded == 2 &&
            moves.get(0).getMove().equals("checkCall") &&
             moves.get(1).getMove().equals("checkCall") &&
                     moves.get(2).getMove().equals("betRaise"))
            {
                roundOfBetting();
            }
        else if(roundOfBetting != 1 && (moves.size() > 3) && folded == 0 &&
            moves.get(0).getMove().equals("checkCall") &&
                    moves.get(1).getMove().equals("checkCall") &&
                     moves.get(2).getMove().equals("checkCall") &&
                     (moves.get(3).getMove().equals("checkCall") ||
                     moves.get(3).getMove().equals("betRaise")))
        {
            roundOfBetting();
        }
        else if(roundOfBetting != 1 && (moves.size() > 2) && folded == 1 &&
            moves.get(0).getMove().equals("checkCall") &&
                    moves.get(1).getMove().equals("checkCall") &&
                     (moves.get(2).getMove().equals("checkCall") ||
                     moves.get(2).getMove().equals("betRaise")))
        {
            roundOfBetting();
        }
        else if (roundOfBetting != 1 && (moves.size() > 1) && folded == 2 &&
            moves.get(0).getMove().equals("checkCall") &&
                     (moves.get(1).getMove().equals("checkCall") ||
                     moves.get(1).getMove().equals("betRaise")))
            {
                roundOfBetting();
            }
        else if(roundOfBetting == 1 && (moves.size() > 3) && folded == 0 &&
            moves.get(0).getMove().equals("checkCall") &&
                    moves.get(1).getMove().equals("checkCall") &&
                     moves.get(2).getMove().equals("checkCall") &&
                     (moves.get(3).getMove().equals("checkCall") ||
                     moves.get(3).getMove().equals("betRaise")) &&
                     (currentBet > 50))
        {
            roundOfBetting();
        }
        else if(roundOfBetting == 1 && (moves.size() > 2) && folded == 1 &&
            moves.get(0).getMove().equals("checkCall") &&
                    moves.get(1).getMove().equals("checkCall") &&
                     (moves.get(2).getMove().equals("checkCall") ||
                     moves.get(2).getMove().equals("betRaise")) &&
                     (currentBet > 50))
        {
            roundOfBetting();
        }
        else if (roundOfBetting == 1 && (moves.size() > 1) && folded == 2 &&
            moves.get(0).getMove().equals("checkCall") &&
                     (moves.get(1).getMove().equals("checkCall") ||
                     moves.get(1).getMove().equals("betRaise"))  &&
                     (currentBet > 50))
            {
                roundOfBetting();
            }
        else
        {
            if(nextToPlay.equals("player1"))
            {
                if(!p2HasFolded)
                    nextToPlay = "player2";
                else if(!p3HasFolded)
                    nextToPlay = "player3";
                else
                    nextToPlay = "player4";
            }
            else if(nextToPlay.equals("player2"))
            {
                if(!p3HasFolded)
                    nextToPlay = "player3";
                else if(!p4HasFolded)
                    nextToPlay = "player4";
                else
                    nextToPlay = "player1";
            }
            else if(nextToPlay.equals("player3"))
            {
                if(!p4HasFolded)
                    nextToPlay = "player4";
                else if(!p1HasFolded)
                    nextToPlay = "player1";
                else
                    nextToPlay = "player2";
            }
            else
            {
                if(!p1HasFolded)
                    nextToPlay = "player1";
                else if(!p2HasFolded)
                    nextToPlay = "player2";
                else
                    nextToPlay = "player3";
            }
            server.sendToClient(nextToPlay, "Your Turn");
        }
    }
    public void setName(String player, String name)
    {
        if(player.equals("player1"))
            p1Name = name;
        else if(player.equals("player2"))
            p2Name = name;
        else if(player.equals("player3"))
            p3Name = name;
        else p4Name = name;
    }

    public void start()
    {
        Sound sound = new Sound();
        p1 = new Player(p1Name);
        p2 = new Player(p2Name);
        p3 = new Player(p3Name);
        p4 = new Player(p4Name);

        d = new Deck();
        pCards = new ArrayList(8);
        comCards = new ArrayList(5);

        sound.playSound();
        newHand();
    }

    public boolean getDone()
    {
        return done;
    }

    public void newHand()
    {
            if(dealer == 4)
                dealer = 1;
            else dealer++;

            p1PrevBet = 0;
            p2PrevBet = 0;
            p3PrevBet = 0;
            p4PrevBet = 0;
            currentBet = 0;
            p1HasFolded = false;
            p2HasFolded = false;
            p3HasFolded = false;
            p4HasFolded = false;
            roundOfBetting = 1;

            done = false;
            currentBet = 0;
            d.restart();
            pCards.clear();
            comCards.clear();
            moves.clear();

            if(dealer == 1)
                actionPlayer = "player4";
            else if(dealer == 2)
                actionPlayer = "player1";
            else if(dealer == 3)
                actionPlayer = "player2";
            else actionPlayer = "player3";

            nextToPlay = actionPlayer;
 


            for(int i = 0; i < 8; i++)
            {
                pCards.add(d.drawCard());
            }
            for(int j = 0; j < 5; j++)
            {
                comCards.add(d.drawCard());
            }

            Hand h1 = new Hand(pCards.get(0), pCards.get(1), comCards);
            Hand h2 = new Hand(pCards.get(2), pCards.get(3), comCards);
            Hand h3 = new Hand(pCards.get(4), pCards.get(5), comCards);
            Hand h4 = new Hand(pCards.get(6), pCards.get(7), comCards);

            p1.setHand(h1);
            p2.setHand(h2);
            p3.setHand(h3);
            p4.setHand(h4);

            table = new Table(p1, p2, p3, p4);
            sendToAll(table);

            server.sendToAll("Now taking antes");
            sleep(1000);
            takeAntes(dealer);

            server.sendToClient(nextToPlay, "Your Turn");
    }
    private void roundOfBetting()
    {
        moves.clear();
        if(roundOfBetting == 1)
        {

            p1PrevBet = 0;
            p2PrevBet = 0;
            p3PrevBet = 0;
            p4PrevBet = 0;
            currentBet = 0;
            table.flipFlop();
            sendToAll(table);
            sleep(1000);
            roundOfBetting = 2;
            server.sendToAll("New Round of Betting");

            if(dealer == 1)
                actionPlayer = "player1";
            else if(dealer == 2)
                actionPlayer = "player2";
            else if(dealer == 3)
                actionPlayer = "player3";
            else actionPlayer = "player4";
            
            nextToPlay = actionPlayer;
            
            if(nextToPlay.equals("player1"))
            {
                if(!p2HasFolded)
                    nextToPlay = "player2";
                else if(!p3HasFolded)
                    nextToPlay = "player3";
                else
                    nextToPlay = "player4";
            }
            else if(nextToPlay.equals("player2"))
            {
                if(!p3HasFolded)
                    nextToPlay = "player3";
                else if(!p4HasFolded)
                    nextToPlay = "player4";
                else
                    nextToPlay = "player1";
            }
            else if(nextToPlay.equals("player3"))
            {
                if(!p4HasFolded)
                    nextToPlay = "player4";
                else if(!p1HasFolded)
                    nextToPlay = "player1";
                else
                    nextToPlay = "player2";
            }
            else
            {
                if(!p1HasFolded)
                    nextToPlay = "player1";
                else if(!p2HasFolded)
                    nextToPlay = "player2";
                else
                    nextToPlay = "player3";
            }


            server.sendToClient(nextToPlay, "Your Turn");
        }
        else if(roundOfBetting == 2)
        {
            p1PrevBet = 0;
            p2PrevBet = 0;
            p3PrevBet = 0;
            p4PrevBet = 0;
            currentBet = 0;
            table.flipTurn();
            sendToAll(table);
            sleep(1000);
            server.sendToAll("New Round of Betting");
            roundOfBetting = 3;

            if(dealer == 1)
                actionPlayer = "player1";
            else if(dealer == 2)
                actionPlayer = "player2";
            else if(dealer == 3)
                actionPlayer = "player3";
            else actionPlayer = "player4";
            
            nextToPlay = actionPlayer;

            if(nextToPlay.equals("player1"))
            {
                if(!p2HasFolded)
                    nextToPlay = "player2";
                else if(!p3HasFolded)
                    nextToPlay = "player3";
                else
                    nextToPlay = "player4";
            }
            else if(nextToPlay.equals("player2"))
            {
                if(!p3HasFolded)
                    nextToPlay = "player3";
                else if(!p4HasFolded)
                    nextToPlay = "player4";
                else
                    nextToPlay = "player1";
            }
            else if(nextToPlay.equals("player3"))
            {
                if(!p4HasFolded)
                    nextToPlay = "player4";
                else if(!p1HasFolded)
                    nextToPlay = "player1";
                else
                    nextToPlay = "player2";
            }
            else
            {
                if(!p1HasFolded)
                    nextToPlay = "player1";
                else if(!p2HasFolded)
                    nextToPlay = "player2";
                else
                    nextToPlay = "player3";
            }

            nextToPlay = actionPlayer;
            server.sendToClient(nextToPlay, "Your Turn");

        }
        else if(roundOfBetting == 3)
        {
            p1PrevBet = 0;
            p2PrevBet = 0;
            p3PrevBet = 0;
            p4PrevBet = 0;
            currentBet = 0;
            table.flipRiver();
            sendToAll(table);
            sleep(1000);
            server.sendToAll("New Round of Betting");
            roundOfBetting = 4;

            if(dealer == 1)
                actionPlayer = "player1";
            else if(dealer == 2)
                actionPlayer = "player2";
            else if(dealer == 3)
                actionPlayer = "player3";
            else actionPlayer = "player4";
            
            nextToPlay = actionPlayer;
            
            if(nextToPlay.equals("player1"))
            {
                if(!p2HasFolded)
                    nextToPlay = "player2";
                else if(!p3HasFolded)
                    nextToPlay = "player3";
                else
                    nextToPlay = "player4";
            }
            else if(nextToPlay.equals("player2"))
            {
                if(!p3HasFolded)
                    nextToPlay = "player3";
                else if(!p4HasFolded)
                    nextToPlay = "player4";
                else
                    nextToPlay = "player1";
            }
            else if(nextToPlay.equals("player3"))
            {
                if(!p4HasFolded)
                    nextToPlay = "player4";
                else if(!p1HasFolded)
                    nextToPlay = "player1";
                else
                    nextToPlay = "player2";
            }
            else
            {
                if(!p1HasFolded)
                    nextToPlay = "player1";
                else if(!p2HasFolded)
                    nextToPlay = "player2";
                else
                    nextToPlay = "player3";
            }

            server.sendToClient(nextToPlay, "Your Turn");

        }
        else
        {
            p1PrevBet = 0;
            p2PrevBet = 0;
            p3PrevBet = 0;
            p4PrevBet = 0;
            currentBet = 0;
            sendToAll(table);
            sleep(1000);
            done = true;
        }
    }

    public void calculateWinner(Table t)
    {
        Table clone = t.clone();
        Table c1;
        Table c2;
        Table c3;
        Table c4;
        HandEvaluator he = new HandEvaluator();
        long p1Score = 0;
        long p2Score = 0;
        long p3Score = 0;
        long p4Score = 0;
        String winner = "";
        int tieWinAmt = 0;
        ArrayList<String> tie = new ArrayList();
        ArrayList<String> tieWinners = new ArrayList();
        Sound sound = new Sound();



        if(!p1HasFolded)
        {
            clone.getPlayer1().getHand().getCard1().flipCard();
            clone.getPlayer1().getHand().getCard2().flipCard();
            p1Score = he.getHandValue(p1.getFullHand());
        }
        if(!p2HasFolded)
        {
            clone.getPlayer2().getHand().getCard1().flipCard();
            clone.getPlayer2().getHand().getCard2().flipCard();
            p2Score = he.getHandValue(p2.getFullHand());
        }
        if(!p3HasFolded)
        {
            clone.getPlayer3().getHand().getCard1().flipCard();
            clone.getPlayer3().getHand().getCard2().flipCard();
            p3Score = he.getHandValue(p3.getFullHand());
        }
        if(!p4HasFolded)
        {
            clone.getPlayer4().getHand().getCard1().flipCard();
            clone.getPlayer4().getHand().getCard2().flipCard();
            p4Score = he.getHandValue(p4.getFullHand());
        }

        c1 = clone.clone();
        c2 = clone.clone();
        c3 = clone.clone();
        c4 = clone.clone();

        if(p1HasFolded)
        {
            c1.getPlayer1().getHand().getCard1().flipCard();
            c1.getPlayer1().getHand().getCard2().flipCard();
        }
        if(p2HasFolded)
        {
            c2.getPlayer2().getHand().getCard1().flipCard();
            c2.getPlayer2().getHand().getCard2().flipCard();
        }
        if(p3HasFolded)
        {
            c3.getPlayer3().getHand().getCard1().flipCard();
            c3.getPlayer3().getHand().getCard2().flipCard();
        }
        if(p4HasFolded)
        {
            c4.getPlayer4().getHand().getCard1().flipCard();
            c4.getPlayer4().getHand().getCard2().flipCard();
        }

        server.sendToClient("player1", c1);
        server.sendToClient("player2", c2);
        server.sendToClient("player3", c3);
        server.sendToClient("player4", c4);

        sleep(2000);

        if((p1Score == p2Score || p1Score == p3Score || p1Score == p4Score) && p1Score != 0)
        {
            tie.add("player1");
        }

        if((p2Score == p1Score || p2Score == p3Score || p2Score == p4Score) && p2Score != 0)
        {
            tie.add("player2");
        }

        if((p3Score == p2Score || p3Score == p1Score || p3Score == p4Score) && p3Score != 0)
        {
            tie.add("player3");
        }

        if((p4Score == p2Score || p4Score == p3Score || p4Score == p1Score) && p4Score != 0)
        {
            tie.add("player4");
        }

        if(!tie.isEmpty())
        {
            if(tie.size() == 2)
            {
                tieWinAmt = (int)(t.getPot() / 2);
                t.clearPot();
                if(tie.get(0).equals("player1") || tie.get(1).equals("player1"))
                {
                    t.getPlayer1().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer1().getName());

                }
                if(tie.get(0).equals("player2") || tie.get(1).equals("player2"))
                {
                    t.getPlayer2().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer2().getName());

                }
                if(tie.get(0).equals("player3") || tie.get(1).equals("player3"))
                {
                    t.getPlayer3().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer3().getName());

                }
                if(tie.get(0).equals("player4") || tie.get(1).equals("player4"))
                {
                    t.getPlayer4().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer4().getName());

                }

                server.sendToAll("There is a tie! " + tieWinners.get(0) +
                        " and " + tieWinners.get(1) +
                        " both WIN and split the pot!");
            }
            else if(tie.size() == 3)
            {
                tieWinAmt = (int)(t.getPot() / 3);
                t.clearPot();
                if(tie.get(0).equals("player1") || tie.get(1).equals("player1")
                        || tie.get(2).equals("player1"));
                {
                    t.getPlayer1().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer1().getName());

                }
                if(tie.get(0).equals("player2") || tie.get(1).equals("player2")
                        || tie.get(2).equals("player2"));
                {
                    t.getPlayer2().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer2().getName());

                }
                if(tie.get(0).equals("player3") || tie.get(1).equals("player3")
                        || tie.get(2).equals("player3"));
                {
                    t.getPlayer3().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer3().getName());

                }
                if(tie.get(0).equals("player4") || tie.get(1).equals("player4")
                        || tie.get(2).equals("player4"));
                {
                    t.getPlayer4().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer4().getName());

                }

                server.sendToAll("There is a tie! " + tieWinners.get(0) +
                        ", " + tieWinners.get(1) + " and " + tieWinners.get(2) +
                        " all WIN and split the pot!");

            }
            else
            {
                tieWinAmt = (int)(t.getPot() / 4);
                t.clearPot();
                if(tie.get(0).equals("player1") || tie.get(1).equals("player1")
                        || tie.get(2).equals("player1") || tie.get(3).equals("player1"));
                {
                    t.getPlayer1().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer1().getName());

                }
                if(tie.get(0).equals("player2") || tie.get(1).equals("player2")
                        || tie.get(2).equals("player2") || tie.get(3).equals("player2"));
                {
                    t.getPlayer2().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer2().getName());

                }
                if(tie.get(0).equals("player3") || tie.get(1).equals("player3")
                        || tie.get(2).equals("player3") || tie.get(3).equals("player3"));
                {
                    t.getPlayer3().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer3().getName());

                }
                if(tie.get(0).equals("player4") || tie.get(1).equals("player4")
                        || tie.get(2).equals("player4") || tie.get(3).equals("player4"));
                {
                    t.getPlayer4().addChips(tieWinAmt);
                    tieWinners.add(t.getPlayer4().getName());

                }

                server.sendToAll("There is a tie! " + tieWinners.get(0) +
                        ", " + tieWinners.get(1) + ", " + tieWinners.get(2) +
                        " and " + tieWinners.get(3) + " all WIN and split the pot!");

            }
        }
        else
        {
            if(p1Score > p2Score && p1Score > p3Score && p1Score > p4Score)
            {
                t.getPlayer1().addChips(t.getPot());
                t.clearPot();
                server.sendToAll(p1.getName() + " WINS!");
            }
            else if(p2Score > p1Score && p2Score > p3Score && p2Score > p4Score)
            {
                t.getPlayer2().addChips(t.getPot());
                t.clearPot();
                server.sendToAll(p2.getName() + " WINS!");
            }
            else if(p3Score > p2Score && p3Score > p1Score && p3Score > p4Score)
            {
                t.getPlayer3().addChips(t.getPot());
                t.clearPot();
                server.sendToAll(p3.getName() + " WINS!");
            }
            else
            {
                t.getPlayer4().addChips(t.getPot());
                t.clearPot();
                server.sendToAll(p4.getName() + " WINS!");
            }
        }

        sleep(5000);
        server.sendToAll("Starting a new hand of poker");
        sleep(1000);
        sound.playSound();
    }

    private void sleep(int x)
    {
        try
            {
                Thread.currentThread().sleep(x);
            }
            catch (Exception e)
            {
            }
    }

    private void takeAntes(int dealer)
    {
        if(dealer == 1)
        {
            table.addToPot(SBLIND + BBLIND);
            p2.takeChips(SBLIND);
            p2PrevBet = SBLIND;
            p3.takeChips(BBLIND);
            p3PrevBet = BBLIND;
        }
        else if(dealer ==2)
        {
            table.addToPot(SBLIND + BBLIND);
            p3.takeChips(SBLIND);
            p3PrevBet = SBLIND;
            p4.takeChips(BBLIND);
            p4PrevBet = BBLIND;
        }
        else if(dealer == 3)
        {
            table.addToPot(SBLIND + BBLIND);
            p4.takeChips(SBLIND);
            p4PrevBet = SBLIND;
            p1.takeChips(BBLIND);
            p1PrevBet = BBLIND;
        }
        else
        {
            table.addToPot(SBLIND + BBLIND);
            p1.takeChips(SBLIND);
            p1PrevBet = SBLIND;
            p2.takeChips(BBLIND);
            p2PrevBet = BBLIND;
        }
        moves.add(0, new Move("betRaise", 25));
        moves.add(0, new Move("checkCall"));
        currentBet = BBLIND;
        sendToAll(table);
    }

    public void removePlayer(String str)
    {

    }

    public void clientsMove(Object msg, String str)
    {

    }

    public Table getTable()
    {
        //Code below is just setting up a random table

//        Deck d = new Deck();
//        ArrayList<Card> pCards = new ArrayList(8);
//        ArrayList<Card> comCards = new ArrayList(5);
//        for(int i = 0; i < 8; i++)
//        {
//            pCards.add(d.drawCard());
//        }
//        for(int j = 0; j < 5; j++)
//        {
//            comCards.add(d.drawCard());
//        }
//
//        Hand h1 = new Hand(pCards.get(0), pCards.get(1), comCards);
//        Hand h2 = new Hand(pCards.get(2), pCards.get(3), comCards);
//        Hand h3 = new Hand(pCards.get(4), pCards.get(5), comCards);
//        Hand h4 = new Hand(pCards.get(6), pCards.get(7), comCards);
//
//        Player p1 = new Player("Bob");
//        Player p2 = new Player("John");
//        Player p3 = new Player("Tim");
//        Player p4 = new Player("Ron");
//
//        p1.setHand(h1);
//        p2.setHand(h2);
//        p3.setHand(h3);
//        p4.setHand(h4);
//
//        Table t = new Table(p1, p2, p3, p4);

        return table;
    }
    
    private void sendToPlayer(Table t, String name)
    {
        Table clone = t.clone();
        
        if(name.equals("player1"))
        {
            clone.getPlayer1().getHand().getCard1().flipCard();
            clone.getPlayer1().getHand().getCard2().flipCard();
        }
        else if(name.equals("player2"))
        {
            clone.getPlayer2().getHand().getCard1().flipCard();
            clone.getPlayer2().getHand().getCard2().flipCard();
        }
        else if(name.equals("player3"))
        {
            clone.getPlayer3().getHand().getCard1().flipCard();
            clone.getPlayer3().getHand().getCard2().flipCard();
        }
        else if(name.equals("player4"))
        {
            clone.getPlayer4().getHand().getCard1().flipCard();
            clone.getPlayer4().getHand().getCard2().flipCard();
        }
        else System.err.println("Error: SendToPlayer - no such player");

        server.sendToClient(name, clone);
    }

    private void sendToAll(Table t)
    {
        Table c1 = t.clone();
        Table c2 = t.clone();
        Table c3 = t.clone();
        Table c4 = t.clone();

        c1.getPlayer1().getHand().getCard1().flipCard();
        c1.getPlayer1().getHand().getCard2().flipCard();

        c2.getPlayer2().getHand().getCard1().flipCard();
        c2.getPlayer2().getHand().getCard2().flipCard();

        c3.getPlayer3().getHand().getCard1().flipCard();
        c3.getPlayer3().getHand().getCard2().flipCard();

        c4.getPlayer4().getHand().getCard1().flipCard();
        c4.getPlayer4().getHand().getCard2().flipCard();

        server.sendToClient("player1", c1);
        server.sendToClient("player2", c2);
        server.sendToClient("player3", c3);
        server.sendToClient("player4", c4);

    }

    private Player p1;
    private Player p2;
    private Player p3;
    private Player p4;
    private Table table;
    private Deck d;
    private String p1Name;
    private String p2Name;
    private String p3Name;
    private String p4Name;
    private String nextToPlay;
    private String actionPlayer;
    private boolean p1HasFolded = false;
    private boolean p2HasFolded = false;
    private boolean p3HasFolded = false;
    private boolean p4HasFolded = false;
    private boolean done = false;
    private int currentBet = 0;
    private int dealer = 0;
    private int p1PrevBet = 0;
    private int p2PrevBet = 0;
    private int p3PrevBet = 0;
    private int p4PrevBet = 0;
    private int roundOfBetting = 1;
    private PokerServer server;
    private ArrayList<Move> moves = new ArrayList();
    ArrayList<Card> comCards;
    ArrayList<Card> pCards;
}