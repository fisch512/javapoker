/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package poker;

import java.awt.event.ActionListener;
import poker_client.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;
import java.util.*;

/**
 *
 * @author Dave
 */
public class ButtonPanel extends JPanel {
    public ButtonPanel(final ClientFrame frame)
    {
        this.frame = frame;
        setLayout(null);
        setSize(500, 200);
        setBackground(Color.BLUE);

        JButton callButton = new JButton("Check/Call");
        JButton raiseButton = new JButton("Bet/Raise");
        JButton foldButton  = new JButton("Fold");
        final JTextField raiseAmt = new JTextField();

        callButton.setFont(new Font("Serif",Font.BOLD,26));
        raiseButton.setFont(new Font("Serif",Font.BOLD,26));
        foldButton.setFont(new Font("Serif",Font.BOLD,26));
        raiseAmt.setFont(new Font("Serif",Font.BOLD,32));
        
        callButton.setBounds(35, 15, 170, 70);
        foldButton.setBounds(35, 115, 170, 70);
        raiseButton.setBounds(285, 115, 170, 70);
        raiseAmt.setBounds(285, 15, 170, 70);


        callButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        frame.pass(new Move("checkCall"));
                    }
                });
        raiseButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        String s = raiseAmt.getText();
                        int x = 0;
                        try
                        {
                            x = Integer.parseInt(s);
                            frame.pass(new Move("betRaise", x));
                        }
                        catch (NumberFormatException ec)
                        {
                            System.err.println("Error bet/raise button");
                            System.err.println(ec);
                        }
                    }
                });
        foldButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        frame.pass(new Move("fold"));
                    }
                });

        add(callButton);
        add(foldButton);
        add(raiseButton);
        add(raiseAmt);
    }
    private ClientFrame frame;
}
