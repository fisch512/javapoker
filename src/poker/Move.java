/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package poker;

import java.io.Serializable;

/**
 *
 * @author Matt
 */
public class Move  implements Serializable {
    public Move(String move)
    {
        this.move = move;
    }

    public Move(String move, int amt)
    {
        this.move = move;
        this.amt = amt;
    }

    public String getMove()
    {
        return move;
    }

    public int getAmt()
    {
        return amt;
    }

    private String move;
    private int amt;
}
