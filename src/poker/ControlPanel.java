package poker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import poker_server.*;
import poker_client.*;

public class ControlPanel extends JPanel {

    private static final Color COLOR = Color.green;
    
    public ControlPanel()
    {
        setLayout(new BorderLayout());
        setBackground(COLOR);
        
        JLabel heading = new JLabel("Texas Hold'em", SwingConstants.CENTER);
        heading.setFont(new Font("Serif", Font.BOLD, 38));

        
        JButton serverButton = new JButton("Launch Server");
        serverButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent event) {
              new ServerFrame(new PokerServer());
           }
        }); 
        
        JButton clientButton = new JButton("Launch Client");
        clientButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent event) {
               new ClientFrame();
           }
        });
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setBackground(COLOR);
        buttonPanel.add(serverButton);
        buttonPanel.add(clientButton);
        
        add(heading, BorderLayout.NORTH);
        add(buttonPanel, BorderLayout.CENTER);
    }
}
