package poker;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class MessagePanel extends JPanel {
    
    public MessagePanel() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setSize(300, 200);
        messageArea = new JTextArea(10,30);
        messageArea.setEditable(false);
        messageArea.setLineWrap(true);
        messageArea.setWrapStyleWord(true);
        messageArea.setFont(new Font("Serif", Font.BOLD, 12));
        scrollPane = 
           new JScrollPane(messageArea,
                           ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                           ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBorder(new TitledBorder("Player Actions"));
        add(scrollPane);
    }
    
   
    public void addMessage(String message) {
        messageArea.append(message + "\n");
        int height = messageArea.getHeight();
        JViewport viewport = scrollPane.getViewport();
        int viewHeight = viewport.getSize().height;
        if (height > viewHeight) {
            viewport.setViewPosition(new Point(0, height-viewHeight));
            repaint();
        }
    }
    
    
    public void clear() {
        messageArea.setText("");
    }
    
    private JTextArea messageArea;
    
    private JScrollPane scrollPane;

}
