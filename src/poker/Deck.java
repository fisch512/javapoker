package poker;

import java.util.*;
import java.awt.image.BufferedImage;
import javax.imageio.*;
import java.io.*;
import javax.swing.ImageIcon;

/**
 * Class that represents a deck of 52 playing cards.
 * @author Matt
 */
public class Deck {

    /**
     * The public contructor for a deck of playing cards. It creates the 52
     * individual cards and then shuffles the deck.
     */
    Deck()
    {
        int count = 1;
        String[] suits = {"Club", "Spade", "Heart", "Diamond"};
        ImageIcon icon = null;
        for(int i = 14; i > 1; i--)
        {
            for(String s : suits)
            {
                icon = new ImageIcon("src/classic-cards/" + count + ".png");
                deck.add(new Card(i, s, icon, false));
                count++;
            }
        }
        shuffleDeck();
    }

    /**
     * Draws a card from the deck.
     * @return the first card from the deck.
     */
    public Card drawCard()
    {
        drawCount++;
        return deck.get(drawCount);
    }

    public Card getCard(int i)
    {
        return deck.get(i - 1);
    }

    public void restart()
    {
        drawCount = -1;
        shuffleDeck();

        for(int i = 0; i < 52; i++)
        {
            if(deck.get(i).isFlippedUp())
                deck.get(i).flipCard();
        }
    }

    private void shuffleDeck()
    {
        for(int i = 0; i < 100; i++)
        {
            Collections.shuffle(deck);
        }
    }

    private ArrayList<Card> deck = new ArrayList<Card>(52);
    private int drawCount = -1;
}
