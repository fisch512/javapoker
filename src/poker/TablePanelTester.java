/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package poker;

import poker_client.*;
import javax.swing.*;
import javax.imageio.*;
import java.awt.*;
import java.io.*;
import java.util.*;

/**
 *
 * @author Matt
 */
public class TablePanelTester {

    public static void main(String[] args)
    {
        Deck d = new Deck();
        ArrayList<Card> pCards = new ArrayList(8);
        ArrayList<Card> comCards = new ArrayList(5);
        for(int i = 0; i < 8; i++)
        {
            pCards.add(d.drawCard());
        }
        for(int j = 0; j < 5; j++)
        {
            comCards.add(d.drawCard());
        }

        Hand h1 = new Hand(pCards.get(0), pCards.get(1), comCards);
        Hand h2 = new Hand(pCards.get(2), pCards.get(3), comCards);
        Hand h3 = new Hand(pCards.get(4), pCards.get(5), comCards);
        Hand h4 = new Hand(pCards.get(6), pCards.get(7), comCards);

        Player p1 = new Player("Bob");
        Player p2 = new Player("John");
        Player p3 = new Player("Tim");
        Player p4 = new Player("Ron");

        p1.setHand(h1);
        p2.setHand(h2);
        p3.setHand(h3);
        p4.setHand(h4);

        Table t = new Table(p1, p2, p3, p4);

        JFrame frame = new JFrame("TablePanel Tester");
        TablePanel panel = new TablePanel(t);
        frame.add(panel);
        frame.pack();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setVisible(true);

        try {
             Thread.currentThread().sleep(1000);
            }
        catch (InterruptedException e) {
             e.printStackTrace();
            }
        p1.addChips(1000);
        p1.getHand().getCard1().flipCard();
        p1.getHand().getCard2().flipCard();
        panel.updateTable(t);
    }

}
