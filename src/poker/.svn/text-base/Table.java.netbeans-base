package poker;

import java.io.*;
import java.util.*;

/**
 * Class that represents the poker table holding the pot and player info.
 * @author Matt
 */
public class Table implements Serializable, Cloneable{


    public Table()
    {
        isEmpty = true;
    }

    /**
     * Public constructor for the table object
     * @param p1 Player 1
     * @param p2 Player 2
     * @param p3 Player 3
     * @param p4 Player 4
     */
    public Table(Player p1, Player p2, Player p3, Player p4)
    {
        isEmpty = false;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;

//        ArrayList<Card> cCards = new ArrayList(p1.getHand().getComCards().size());
//        comCards.addAll(p1.getHand().getComCards());
        comCards = p1.getHand().getComCards();
    }

    public Table clone()
    {
        try
        {
            Table clonedTable = (Table) super.clone();
            clonedTable.p1 = (Player) p1.clone();
            clonedTable.p2 = (Player) p2.clone();
            clonedTable.p3 = (Player) p3.clone();
            clonedTable.p4 = (Player) p4.clone();

            ArrayList<Card> list = new ArrayList();

            for(Card o : comCards)
            {
                list.add(o.clone());
            }

//            for(int i = 0; i < 5; i++)
//            {
//                list.set(i, (Card)comCards.get(i).clone());
//            }
            //list.addAll(comCards);
            clonedTable.comCards = list;

            return clonedTable;
        }
        catch (CloneNotSupportedException e)
        {
            System.err.println("Error: Table Clone");
            return null;
        }
    }

    public boolean getIsEmpty()
    {
        return isEmpty;
    }

    public void clearPot()
    {
        pot = 0;
    }

    /**
     * Returns the community cards.
     * @return community cards.
     */
    public ArrayList<Card> getComCards()
    {
        return comCards;
    }

    /**
     * Flips the first 3 community cards.
     */
    public void flipFlop()
    {
        for(int i = 0; i < 3; i++)
        {
            comCards.get(i).flipCard();
        }
    }

    /**
     * Flips the 4th community card.
     */
    public void flipTurn()
    {
        comCards.get(3).flipCard();
    }

    /**
     * FLips the last community card.
     */
    public void flipRiver()
    {
        comCards.get(4).flipCard();
    }

    /**
     * Returns player 1
     * @return p1
     */
    public Player getPlayer1()
    {
        return p1;
    }

    /**
     * Returns player 2
     * @return p2
     */
    public Player getPlayer2()
    {
        return p2;
    }

    /**
     * Returns player 3
     * @return p3
     */
    public Player getPlayer3()
    {
        return p3;
    }

    /**
     * Returns player4
     * @return p4
     */
    public Player getPlayer4()
    {
        return p4;
    }

    /**
     * Adds to the pot
     * @param ammount the ammount to add to the pot
     */
    public void addToPot(int ammount)
    {
        pot += ammount;
    }

    /**
     * Returns the pot
     * @return the pot
     */
    public int getPot()
    {
        return pot;
    }

    /**
     * Returns the value of the pot and sets it equal to zero
     * @return the pot
     */
    public int takePot()
    {
        int x = pot;
        pot = 0;
        return x;
    }

    private Player p1;
    private Player p2;
    private Player p3;
    private Player p4;
    private int pot;
    private boolean isEmpty = false;
    private ArrayList<Card> comCards = new ArrayList(5);



}
