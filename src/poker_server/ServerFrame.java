package poker_server;

import javax.swing.*;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ServerFrame extends JFrame {
    
    public ServerFrame(final PokerServer server) {
        super("Poker Server");
        this.server = server;
        JPanel panel = new JPanel();
        panel.setBackground(Color.green);
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        JLabel label = new JLabel("ServerStarted   ", SwingConstants.CENTER);
        label.setFont(new Font("Serif", Font.BOLD, 38));

        JButton quitButton = new JButton("CLOSE SERVER");
        quitButton.setFont(new Font("Serif", Font.BOLD, 16));
        quitButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent event) {
                server.sendToAllClients("Server going down...");
                try
                {
                    server.close();
                }
                catch (IOException ex)
                {
                    server.sendToAll("Error Closing Server");
                }
                System.exit(1);
           }
        });

        panel.add(label);
        panel.add(quitButton);
        add(panel);

        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) 
    {
        new ServerFrame(new PokerServer());
    }

    private PokerServer server;
}
